server:
  port: 8847
  servlet:
    session:
      timeout: 1800
  error:
    include-exception: true
    include-message: always
    include-stacktrace: always
    include-binding-errors: always
spring:
  application:
    name:  @artifactId@
  profiles:
    active: postgresql
  main:
    allow-bean-definition-overriding: true
    log-startup-info: false
  data:
    redis:
      repositories:
        enabled: false
  cloud:
    httpclientfactories:
      ok:
        enabled: false
  session:
    store-type: redis

jasypt:
  encryptor:
    password: ${JASPYT_HOME:2bda7d3a-dba1-45a4-b08e-cbd731a0418e}

---
# 开发环境配置
spring:
  config:
    activate:
      on-profile: postgresql
  sql:
    init:
      mode: never
      platform: postgresql
      continue-on-error: true
      encoding: UTF-8
      username: ${spring.datasource.username}
      password: ${spring.datasource.password}
      # 第一种方式启动的时候 Jpa 会自动创建表，import.sql 只负责创建表单后的初始化数据。第二种方式启动的时候不会创建表，需要在初始化脚本中判断表是否存在，再初始化脚本的步骤
      # refer: https://www.cnblogs.com/ityouknow/p/7089170.html
      data-locations:
        - classpath:sqls/uaa-data-postgresql.sql
  #        - classpath:sqls/upms-data-mysql.sql

  datasource:
    driver-class-name: com.p6spy.engine.spy.P6SpyDriver
    url: jdbc:p6spy:postgresql://localhost:15432/athena
    username: athena
    password: athena
    type: com.zaxxer.hikari.HikariDataSource
    hikari:
      # Lowest acceptable connection timeout is 250 ms. Default: 30000 (30 seconds)
      connection-timeout: 5000
      connection-test-query: SELECT 1
      # The minimum allowed value is 10000ms (10 seconds). Default: 600000 (10 minutes)
      idle-timeout: 600000
      minimum-idle: 10
      maximum-pool-size: 20
      # We strongly recommend setting this value, and it should be at least 30 seconds less than any database or infrastructure imposed connection time limit.
      # Default: 1800000 (30 minutes)
      max-lifetime: 1800000
      pool-name: Hikari
      # Lowest acceptable validation timeout is 250 ms. Default: 5000
      validation-timeout: 5000
      data-source-properties:
        prepStmtCacheSize: 250
        prepStmtCacheSqlLimit: 2048
        cachePrepStmts: true
        cacheResultSetMetadata: true
        cacheServerConfiguration: true
        useServerPrepStmts: true
        useLocalSessionState: true
        rewriteBatchedStatement: true
        elideSetAutoCommits: true
        maintainTimeStats: false
  jpa:
    database: postgresql
    generate-ddl: true
    open-in-view: true
    hibernate:
      # DDL mode. This is actually a shortcut for the "hibernate.hbm2ddl.auto" property. Default to "create-drop" when using an embedded database, "none" otherwise.spring.jpa.hibernate.ddl-auto=update
      ddl-auto: update
      naming:
        physical-strategy: cn.herodotus.engine.data.jpa.hibernate.HerodotusPhysicalNamingStrategy
    properties:
      javax:
        persistence:
          sharedCache:
            mode: ENABLE_SELECTIVE
      hibernate:
        dialect: org.hibernate.dialect.PostgreSQL10Dialect
        format_sql: true
        show_sql: false
        generate_statistics: false
        cache:
          use_query_cache: true
          use_second_level_cache: true
          region:
            factory_class: cn.herodotus.engine.data.jpa.hibernate.cache.spi.HerodotusRegionFactory
        javax:
          cache:
            missing_cache_strategy: create
        temp:
          use_jdbc_metadata_defaults: false
  redis:
    database: 3
    host: 192.168.101.10
    password:
    port: 16379
    timeout: 10000
    # 如果使用的jedis 则将lettuce改成jedis即可
    lettuce:
      pool:
        # 最大活跃链接数 默认8
        max-active: 10
        max-wait: 10000
        # 最大空闲连接数 默认8
        max-idle: 10
        # 最小空闲连接数 默认0
        min-idle: 5
    redisson:
      enabled: false
      single-server-config:
        address: redis://${spring.redis.host}:${spring.redis.port}
  #        password: ${spring.redis.password}
  thymeleaf:
    mode: HTML
    cache: false
  security:
    oauth2:
      resourceserver:
        opaquetoken:
          client-id: 14a9cf797931430896ad13a6b1855611
          client-secret: a05fe1fc50ed42a4990c6c6fc4bec398

springdoc:
  api-docs:
    resolve-schema-properties: true
  swagger-ui:
    disable-swagger-default-url: true
    operations-sorter: method
    show-common-extensions: true
    show-extensions: true
    oauth:
      use-pkce-with-authorization-code-grant: false
      app-name: ${spring.application.name}


mybatis-plus:
  global-config:
    banner: false
    db-config:
      table-underline: true
  configuration:
    cache-enabled: true
    map-underscore-to-camel-case: true

jetcache:
  statIntervalMinutes: 15
  areaInCacheName: false
  local:
    default:
      type: caffeine
      keyConvertor: fastjson2
      limit: 10000
  remote:
    default:
      type: redis.lettuce
      keyConvertor: fastjson2
      broadcastChannel: herodotus
      valueEncoder: java
      valueDecoder: java
      poolConfig:
        minIdle: ${spring.redis.lettuce.pool.min-idle}
        maxIdle: ${spring.redis.lettuce.pool.max-idle}
        maxTotal: ${spring.redis.lettuce.pool.max-active}
        maxWait: ${spring.redis.lettuce.pool.max-wait}
      uri:
        - redis://${spring.redis.password}@${spring.redis.host}:${spring.redis.port}/${spring.redis.database}


herodotus:
  platform:
    architecture: monocoque
    swagger:
      # Swagger中不知道哪里包含Kafka的注解，会自动启动，如果不连接到Kafka会导致启动失败。暂时没有解决，先取消掉。
      enabled: true
  endpoint:
    issuer-uri: http://localhost:${server.port}
    gateway-service-uri: ${herodotus.endpoint.issuer-uri}
    uaa-service-uri: ${herodotus.endpoint.issuer-uri}
    upms-service-uri: ${herodotus.endpoint.issuer-uri}
    access-token-uri: ${herodotus.endpoint.issuer-uri}/oauth2/token
  rest:
    scan:
      enabled: true
  cache:
    expires:
      data-upms-sys-employee:
        duration: 2
        unit: hours
  captcha:
    graphics:
      letter: num_and_upper_char
      font: action
  sms:
    enabled: true
    sandbox: true
    test-code: 12345678
    default-channel: ALIYUN
    aliyun:
      enabled: true
      access-key-id: LTAIbpBuZTocddot
      access-key-secret: qqTHQOJRlc7BSqPJMjHnftGH4vmW2W
      region-id: cn-shanghai
      sign-name: braineex
      version: 2017-05-25
      templates: { "VERIFICATION_CODE": "SMS_180052229" }
    recluse:
      enabled: true
      username: aaa
      password: bbb
      cm:
        host: 192.168.101.10
        port: 8888
      ws:
        host: 192.168.101.10
        port: 9999
      templates: { "VERIFICATION_CODE": "VERIFICATION_CODE" }
  access:
    justauth:
      enabled: true
      configs:
        GITEE:
          client-id: 7c1623d76f3909757912338688cae8a061e241b5607
          client-secret: 8c343cad9ca732d54242f002d254239e17b68ca
          redirect-uri: http://aw255/social/oauth2/callback/GITEE
        OSCHINA:
          client-id: 7c1623d76f3909757912338688cae8a061e2
          client-secret: 8c343cad9ca732d54242f002d254239e1
          redirect-uri: http://192.168.101.10:8847/herodotus-cloud-uaa/oauth/social/gitee
  pay:
    alipay:
      enabled: true
      sandbox: true
      cert-mode: false
      default-profile: sandbox
      return-url: http://aw255fkfqy5m.ngrok2.xiaomiqiu.cn/open/pay/alipay/return
      notify-url: http://aw255fkfqy5m.ngrok2.xiaomiqiu.cn/open/pay/alipay/notify
      profiles:
        sandbox:
          app-id: 2021000118687269
          app-private-key: MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQCQ9fVJ8NxA
          alipay-public-key: MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEArAGFETKzeqJ2oYP
  oss:
    minio:
      endpoint: http://127.0.0.1:9000
      access-key: herodotus
      secret-key: herodotus
  nosql:
    couchdb:
      endpoint: http://127.0.0.1:5984
      username: herodotus
      password: herodotus
  multi-tenancy:
    enabled: false
    data-sources:
      TENANT01:
        driver-class-name: com.p6spy.engine.spy.P6SpyDriver
        url: jdbc:p6spy:postgresql://localhost:15432/athena
        username: athena
        password: athena

logging:
  file:
    name: logs/herodotus-cloud-athena.log
  level:
    root: INFO
    org.springframework: info
    org.springframework.security: trace
    org.springframework.web: info
    cn.herodotus: debug
    com.alicp.jetcache: info

---
# 开发环境配置
spring:
  config:
    activate:
      on-profile: mysql8
  sql:
    init:
      mode: never
      platform: mysql
      continue-on-error: false
      encoding: UTF-8
      username: ${spring.datasource.username}
      password: ${spring.datasource.password}
      data-locations:
        #        - classpath:sqls/uaa-data-mysql.sql
        - classpath:sqls/uaa-data-postgresql.sql

  datasource:
    driver-class-name: com.p6spy.engine.spy.P6SpyDriver
    url: jdbc:p6spy:mysql://127.0.0.1:13306/athena?characterEncoding=utf8&connectTimeout=1000&socketTimeout=3000&autoReconnect=true&useUnicode=true&useSSL=false&serverTimezone=UTC&allowPublicKeyRetrieval=true
    username: athena
    password: athena
    type: com.zaxxer.hikari.HikariDataSource
    hikari:
      # Lowest acceptable connection timeout is 250 ms. Default: 30000 (30 seconds)
      connection-timeout: 5000
      connection-test-query: SELECT 1
      # The minimum allowed value is 10000ms (10 seconds). Default: 600000 (10 minutes)
      idle-timeout: 600000
      minimum-idle: 10
      maximum-pool-size: 20
      # We strongly recommend setting this value, and it should be at least 30 seconds less than any database or infrastructure imposed connection time limit.
      # Default: 1800000 (30 minutes)
      max-lifetime: 1800000
      pool-name: Hikari
      # Lowest acceptable validation timeout is 250 ms. Default: 5000
      validation-timeout: 5000
      data-source-properties:
        prepStmtCacheSize: 250
        prepStmtCacheSqlLimit: 2048
        cachePrepStmts: true
        cacheResultSetMetadata: true
        cacheServerConfiguration: true
        useServerPrepStmts: true
        useLocalSessionState: true
        rewriteBatchedStatement: true
        elideSetAutoCommits: true
        maintainTimeStats: false
  jpa:
    database: mysql
    generate-ddl: true
    open-in-view: true
    hibernate:
      # DDL mode. This is actually a shortcut for the "hibernate.hbm2ddl.auto" property. Default to "create-drop" when using an embedded database, "none" otherwise.spring.jpa.hibernate.ddl-auto=update
      ddl-auto: update
      naming:
        physical-strategy: cn.herodotus.engine.data.jpa.hibernate.HerodotusPhysicalNamingStrategy
    properties:
      javax:
        persistence:
          sharedCache:
            mode: ENABLE_SELECTIVE
      hibernate:
        dialect: org.hibernate.dialect.MySQL8Dialect
        format_sql: false
        show_sql: false
        generate_statistics: false
        cache:
          use_query_cache: true
          use_second_level_cache: true
          region:
            factory_class: cn.herodotus.engine.data.jpa.hibernate.cache.spi.HerodotusRegionFactory
        javax:
          cache:
            missing_cache_strategy: create
        temp:
          use_jdbc_metadata_defaults: false
  redis:
    database: 3
    host: 192.168.101.10
    password:
    port: 16379
    timeout: 10000
    # 如果使用的jedis 则将lettuce改成jedis即可
    lettuce:
      pool:
        # 最大活跃链接数 默认8
        max-active: 10
        max-wait: 10000
        # 最大空闲连接数 默认8
        max-idle: 10
        # 最小空闲连接数 默认0
        min-idle: 5
  thymeleaf:
    mode: HTML
    cache: false
    prefix: classpath:/templates
  security:
    oauth2:
      resourceserver:
        opaquetoken:
          client-id: 14a9cf797931430896ad13a6b1855611
          client-secret: a05fe1fc50ed42a4990c6c6fc4bec398

springdoc:
  api-docs:
    resolve-schema-properties: true
  swagger-ui:
    disable-swagger-default-url: true
    operations-sorter: method
    show-common-extensions: true
    show-extensions: true
    oauth:
      use-pkce-with-authorization-code-grant: true
      app-name: ${spring.application.name}

mybatis-plus:
  global-config:
    banner: false
    db-config:
      table-underline: true
  configuration:
    cache-enabled: true
    map-underscore-to-camel-case: true

jetcache:
  statIntervalMinutes: 15
  areaInCacheName: false
  local:
    default:
      type: caffeine
      keyConvertor: fastjson2
      limit: 10000
  remote:
    default:
      type: redis.lettuce
      keyConvertor: fastjson2
      broadcastChannel: herodotus
      valueEncoder: java
      valueDecoder: java
      poolConfig:
        minIdle: ${spring.redis.lettuce.pool.min-idle}
        maxIdle: ${spring.redis.lettuce.pool.max-idle}
        maxTotal: ${spring.redis.lettuce.pool.max-active}
        maxWait: ${spring.redis.lettuce.pool.max-wait}
      uri:
        - redis://${spring.redis.password}@${spring.redis.host}:${spring.redis.port}/${spring.redis.database}

camunda:
  bpm:
    database:
      type: postgres
      schema-update: false
    jpa:
      enabled: true
    job-execution:
      enabled: false

herodotus:
  platform:
    architecture: monocoque
    swagger:
      # Swagger中不知道哪里包含Kafka的注解，会自动启动，如果不连接到Kafka会导致启动失败。暂时没有解决，先取消掉。
      enabled: false
  endpoint:
    issuer-uri: http://localhost:${server.port}
    gateway-service-uri: ${herodotus.endpoint.issuer-uri}
    uaa-service-uri: ${herodotus.endpoint.issuer-uri}
    upms-service-uri: ${herodotus.endpoint.issuer-uri}
    access-token-uri: ${herodotus.endpoint.issuer-uri}/oauth2/token
  rest:
    scan:
      enabled: true
  cache:
    clear-remote-on-exit: true
    expires:
      data-upms-sys-employee:
        duration: 2
        unit: hours
  captcha:
    graphics:
      letter: num_and_upper_char
      font: action
  sms:
    enabled: true
    default-channel: ALIYUN
    aliyun:
      enabled: true
      access-key-id: XXXXXXX
      access-key-secret: XXXXX
      region-id: cn-shanghai
      sign-name: bbbbbbbb
      version: 2017-05-25
      templates: { "VERIFICATION_CODE": "SMS_22222222" }
  access:
    justauth:
      enabled: true
      configs:
        GITEE:
          - client-id: 7c1623d76f3909757912338688cae8a061e2
          - client-secret: 8c343cad9ca732d54242f002d254239e17b
          - redirect-uri: http://192.168.101.10:8847/dante-cloud-uaa/oauth/social/gitee
  multi-tenancy:
    enabled: false
    data-sources:
      TENANT01:
        driver-class-name: com.p6spy.engine.spy.P6SpyDriver
        url: jdbc:p6spy:postgresql://localhost:15432/athena
        username: athena
        password: athena

logging:
  level:
    root: INFO
    org.springframework: INFO
    org.springframework.security: debug
    org.springframework.web: debug
    cn.herodotus.dante: DEBUG
    com.alicp.jetcache: INFO
  file:
    name: logs/dante-cloud-athena.log

---
# 生产环境配置
spring:
  config:
    activate:
      on-profile: production